# وب‌پک (webpack)

این ابزار برای بسته‌بندی مولفه‌های بِرکایمان بکار می‌آید.

## مفهوم‌ها

- ورودی (Entry): یه نقطه‌ی شروع آشکارگر ماژولی است که وب‌پک باید برای ساخت گراف وابستگی‌های داخلی ازش استفاده کند.
- خروجی (Output): برای تعیین مسیر/نام فایل خروجی.
- بارگذار (Loader): چون وب‌پک تنها فایل‌‌های جاوا-اسکریپتی را می‌شناسد، بارگذارها امکان پردازش فایل‌های با نوع‌های دیگر را به وب‌پک می‌دهند.
- افزونه (Plugins): افزودن وظیفه‌های بیشتر (مانند بهینه‌سازی بسته‌ها، مدیریت دارایی و تزریق متغیرهای محیطی) به بارگذار.
- وضعیت (Mode): فعال کردن یکی از بهیننده‌های درونی. مقدارهای مجاز: `development`، `production`، و `none`.

یه مثال از یه پیکربندی ساده:

```javascript
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack"); // دسترسی به افزونه‌های درونی

module.exports = {
  entry: "./src/index.js", // مقدار پیش‌فرض
  //پیش‌فرض: "./dist/main.js"
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "main.js",
  },
  module: {
    rules: [
      {
        test: /\.txt$/, // تعیین فایل تبدیل‌شو
        use: "raw-loader", // تعیین تبدیلنده
      },
    ],
  },
  plugins: [new HtmlWebpackPlugin({ template: "./src/index.html" })],
};
```

## ورودی

در زیر انواع روش‌های پیکربندی ورودی نموده شده است.

### نحو تک ورودی (اختصار)

این یه روش خلاصه نویسی است. در زیر تعبیر این نحو نموده شده است.

```javascript
// رشته یا آرایه‌ای از رشته‌ها
module.exports = {
  entry: "./path/to/my/entry/file.js",
};
// که خلاصه‌ی این است
module.exports = {
  entry: {
    main: "./path/to/my/entry/file.js",
  },
};
```

توجه: اگر آرایه‌ای از مسیرها را در قرار دهیم، فایل‌های وابسته درهم تزریق شده و گراف وابستگی‌هایشان در یه بریده قرار می‌گیرد. این به نام ورودی »multi-main« شناخته می‌شود.

### نحو شی‌ای

برای تعریف چندین بریده در ورودی.

```javascript
module.exports = {
  // entry: { <entryChunkName> string | [string] }
  entry: {
    app: "./src/app.js",
    adminApp: "./src/adminApp.js",
  },
};
```

## خروجی

در خروجی حداقل باید خصیصه‌ی نام فایل تعیین شود.

```javascript
module.exports = {
  output: {
    filename: "bundle.js",
  },
};
```

### چند ورودی

تعیین نام یکتا به ازای هر ورودی:

```javascript
module.exports = {
  entry: {
    app: "./src/app.js",
    search: "./src/search.js",
  },
  output: {
    filename: "[name].js",
    path: __dirname + "/dist",
  },
};
```

## مثال‌های کاربردی

### جداسازی برکا از فروشنده‌ها (Vendors)

```javascript
module.exports = {
  entry: {
    main: "./src/app.js",
    vendor: "./src/vendor.js",
  },
  output: {
    filename: "[name].[contentHash].bundle.js",
  },
};
```

### برکای چند صفحه‌ای

```javascript
module.exports = {
  entry: {
    pageOne: "./src/pageOne/index.js",
    pageTwo: "./src/pageTwo/index.js",
    pageThree: "./src/pageThree/index.js",
  },
  output: {
    filename: "[name].[contentHash].bundle.js",
  },
};
```

## بارگذار

برای اینکه فایل‌های غیر جاواسکریپتی را وارد (`import`) جاواسکریپت کنیم، از بارگذارها استفاده می‌کنیم. مثلا، با بارگذار مناسب می‌توان فایل‌های `css` را وارد جاواسکریپت کرد! و حتی تایپ‌اسکریپت را به جاواسکریپت تبدیل کرد.

### بارکردن `css` و تبدیل تایپ‌اسکریپت به جاواسکریپت

1- نصب بارگذارها

```bash
npm install --save-dev css-loader ts-loader
```

2- پیکربندی

```javascript
module.exports = {
  module: {
    rules: [
      { test: /\.css$/, use: "css-loader" },
      { test: /\.ts$/, use: "ts-loader" },
    ],
  },
};
```

### انواع روش‌های استفاده از بارگذار

1- پیکربندیدن (پیکربندی کردن)

در این روش بارگذارها از چپ به راست و از بالا به پایین اجرا/ارزیابی می‌شوند.

توجه: نتیجه‌ی هر بارگذار به بارگذار بعدی‌اش داده می‌شود.

```javascript
module.exports = {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          // style-loader
          { loader: "style-loader" },
          // css-loader
          {
            loader: "css-loader",
            options: {
              modules: true,
            },
          },
          // sass-loader
          { loader: "sass-loader" },
        ],
      },
    ],
  },
};
```

2- درخط

مشخص کردن یه بارگذار در هنگام وارد کردن. [اطلاعات بیشتر](https://webpack.js.org/concepts/loaders/#inline)

```javascript
import Styles from "style-loader!css-loader?modules!./styles.css";
```

3- CLI

```cli
webpack --module-bind pug-loader --module-bind 'css=style-loader!css-loader'
```

## واژه‌نامه

**نرم‌افزار (Software)**:
یه مجموعه از دستورها که تعامل کاربر با رایانه (جنبه سخت‌افزار) را ممکن می‌کند. یه رایانه بدون نرم‌افزار کاملا بی‌استفاده است. یه مثال از نرم‌افزار سیستم‌عامل‌ها هستند.

**برنامه (Program)**:
یه مفهوم برای سازماندهی دستورهای نرم‌افزار است. هر مجموعه دستور که یه وظیفه یا عملکرد خاصی را انجام می‌دهد، برنامه نامیده می‌شود. مانند، برنامه‌ی واژه پرداز مایکروسافت و کتابخانه‌ها می‌دهند.

**بِرکا (App)**:
یه برنامه کامل که یه عملکرد مشخص را به کاربر نهایی ارایه می‌دهد. همه‌ی برکا‌ها برنامه هستند؛ ولی عکسش درست نیست.

**مولفه (Component)**:
یه مفهوم برای سازماندهی برنامه است. هر برنامه از ترکیب مولفه‌ها ایجاد می‌شود.

**واحد (Module)**:
هر مولفه‌ای که بتواند مستقل کارش را انجام دهد، واحد نامیده می‌شود.

**بقچه (Bundle)**:
یه مولفه یا واحد که از ترکیب مولفه(ها) یا واحد(ها)ی دیگر ساخته شده. در وب‌پک بقچه‌ها برآمده از فرایند بارگذاردن و گردآوردن (compilation) هستند.

**بریده (Chunk)**:
این اصطلاح خاص وب‌پک برای مدیریت بقچیدن (بقچه‌کردن) استفاده‌ی داخلی دارد. هر بقچه متشکل از بریده‌هایی از نوع ورودی (entry) یا فرزند‌ها (child) است. معمولا بریده‌ها با بقچه‌های خروجی نظیر هستند ولی برخی پیکربندی‌ها یه تناظر یک‌به‌یک ایجاد نمی‌کنند.

---

مرجع‌ها

[مستند وب‌پک](https://webpack.js.org/concepts/)
